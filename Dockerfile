FROM rootproject/root:6.24.06-conda

USER root

RUN apt-get update
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y --no-install-recommends \
wget ca-certificates python python-dev gfortran build-essential \
ghostscript nano vim libboost-all-dev rsync gnuplot
RUN wget --quiet -O- https://bootstrap.pypa.io/get-pip.py | python -
RUN pip install numpy scipy

WORKDIR /home/hep

ENV MG_VERSION="MG5_aMC_v3_3_0"
RUN wget --quiet -O- https://launchpad.net/mg5amcnlo/3.0/3.3.x/+download/MG5_aMC_v3.3.0.tar.gz | tar xzf -
WORKDIR /home/hep/${MG_VERSION}

ENV ROOTSYS /opt/conda

# install tools
RUN echo "install lhapdf6" | /home/hep/${MG_VERSION}/bin/mg5_aMC
RUN echo "install pythia8" | /home/hep/${MG_VERSION}/bin/mg5_aMC

# enable autoconvert
RUN echo "set auto_convert_model T" | /home/hep/${MG_VERSION}/bin/mg5_aMC

# disable autoupdate
RUN rm /home/hep/${MG_VERSION}/input/.autoupdate

# set library path
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/home/hep/${MG_VERSION}/HEPTools/lib/

# install default PDF
WORKDIR /home/hep/${MG_VERSION}/HEPTools/lhapdf6/share/LHAPDF
RUN wget --quiet -O- http://lhapdfsets.web.cern.ch/lhapdfsets/current/NNPDF30_lo_as_0130.tar.gz | tar xzf -

WORKDIR /home/hep
CMD /bin/bash