# MadGraph 

This project hosts a docker image and setup instructions for a setup with the MadGraph_aMC@NLO + Pythia8 + Delphes stack.

It can be used as the basis for other docker images containing models and scripts for their analysis.

## Tools

| Task             | Tool             | Version | Download |
| ---------------- | ---------------- | -----   | -------- |
| Event generation | MadGraph_aMC@NLO | 3.3.0   | [MG5_aMC_v3.3.0.tar.gz](https://launchpad.net/mg5amcnlo/3.0/3.3.x/+download/MG5_aMC_v3.3.0.tar.gz) |


## Launch docker image


